package com.bielinski.controllers;

import com.bielinski.models.ChildWindowModel;
import com.bielinski.views.StartWindowView;

import java.util.ArrayList;
import java.util.List;

public class StartWindowController {
    private final int maxChildWindowControllersListSize;
    private List<ChildWindowController> childWindowControllers;
    private ChildWindowModel childWindowModel;

    public StartWindowController(int maxChildWindowControllersListSize) {
        new StartWindowView(this);
        this.maxChildWindowControllersListSize = maxChildWindowControllersListSize;
        childWindowControllers = new ArrayList<>();
    }

    public void openChildWindow() {
        closingToMuchWindows();
        childWindowControllers.add(new ChildWindowController(childWindowModel, this));
    }

    private void closingToMuchWindows() {
        if (childWindowControllers.isEmpty()) {
            childWindowModel = new ChildWindowModel();
        } else if (childWindowControllers.size() > maxChildWindowControllersListSize) {
            for (ChildWindowController childWindowController : childWindowControllers) {
                childWindowController.hideWindow();
            }
            childWindowControllers.clear();
            childWindowControllers = new ArrayList<>();
        }
    }

    void addElementToAllViews(String element) {
        if (!childWindowControllers.isEmpty()) {
            for (ChildWindowController childWindowController : childWindowControllers) {
                childWindowController.addElementToView(element);
            }
        }
    }
}
