package com.bielinski.controllers;

import com.bielinski.models.ChildWindowModel;
import com.bielinski.views.ChildWindowView;

public class ChildWindowController {
    private final StartWindowController startWindowController;
    private final ChildWindowModel childWindowModel;
    private final ChildWindowView childWindowView;

    ChildWindowController(ChildWindowModel childWindowModel, StartWindowController startWindowController) {
        this.startWindowController = startWindowController;
        this.childWindowModel = childWindowModel;
        this.childWindowView = new ChildWindowView(this);
        if (childWindowModel.getData().isEmpty()) childWindowModel.addSomeData();
        showCurrentData();
    }

    private void showCurrentData() {
        childWindowView.showAllData(childWindowModel.getData());
    }

    private void addElementToModel(String element) {
        childWindowModel.addElement(element);
    }

    void addElementToView(String text) {
        childWindowView.showNewElement(text);
        childWindowView.refreshData();
    }

    private void addElementToViews(String element) {
        startWindowController.addElementToAllViews(element);
    }

    public void addElement(String element) {
        element = element.trim();
        if (element.length() > 0) {
            addElementToModel(element);
            addElementToViews(element);
        } else
            childWindowView.showWarningMessage();
    }

    void hideWindow() {
        childWindowView.setVisible(false);
    }
}
