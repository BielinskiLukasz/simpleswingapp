package com.bielinski.models;

import java.util.ArrayList;
import java.util.List;

public class ChildWindowModel {
    private final List<String> data;

    public ChildWindowModel() {
        data = new ArrayList<>();
    }

    public void addSomeData() {
        for (int i = 0; i < ('C' - 'A') + 1; i++) {
            data.add("Element " + (char) ('A' + i));
        }
    }

    public List<String> getData() {
        return data;
    }

    public void addElement(String element) {
        data.add(element);
    }
}
