package com.bielinski.views;

import com.bielinski.controllers.StartWindowController;

import javax.swing.*;
import java.awt.*;

public class StartWindowView extends JFrame {
    private final StartWindowController startWindowController;

    public StartWindowView(StartWindowController controller) {
        this.startWindowController = controller;

        setTitle("Start Window");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints(); //to organisation axis of added elements

        JLabel jLabel = new JLabel("Hello in my GUI application");
        JButton jButton = new JButton("Open window");
        gridBagConstraints.insets = new Insets(10, 5, 10, 5); //set up insets between elements
        add(jLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1; //change y axis for next added element
        add(jButton, gridBagConstraints);

        jButton.addActionListener(e -> startWindowController.openChildWindow());

        setVisible(true);
        getContentPane().setPreferredSize(new Dimension(200, 200));
        pack();
        setMinimumSize(new Dimension(180, 110));
    }
}
