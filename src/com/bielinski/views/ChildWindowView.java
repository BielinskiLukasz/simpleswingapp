package com.bielinski.views;

import com.bielinski.controllers.ChildWindowController;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ChildWindowView extends JFrame {
    private final JPanel jPanel;
    private final JScrollBar jScrollPaneVertical;

    public ChildWindowView(ChildWindowController childWindowController) {
        //Create and set up the window.
        setTitle("Child Window");

        //Set up the content pane.
        Container contentPane = getContentPane();
        SpringLayout layout = new SpringLayout();
        contentPane.setLayout(layout);

        //Create and add the components.
        jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        JScrollPane jScrollPane = new JScrollPane(jPanel);
        jScrollPaneVertical = jScrollPane.getVerticalScrollBar();
        JTextField jTextField = new JTextField("", 30);
        JButton jButton = new JButton("Add");
        contentPane.add(jScrollPane);
        contentPane.add(jTextField);
        contentPane.add(jButton);

        //Create button action.
        jButton.addActionListener(e -> {
            actionAddingElement(jTextField, childWindowController);
            jTextField.grabFocus();
        });

        //Create text field action.
        jTextField.addActionListener(e -> actionAddingElement(jTextField, childWindowController));

        //Adjust constraints for elements and set basic localisations + padding.
        SpringLayout.Constraints jScrollPaneConstrains = layout.getConstraints(jScrollPane);
        SpringLayout.Constraints jTextFieldConstraints = layout.getConstraints(jTextField);
        SpringLayout.Constraints jButtonConstraints = layout.getConstraints(jButton);
        jScrollPaneConstrains.setY(Spring.constant(5));
        jTextFieldConstraints.setX(Spring.constant(5));
        jButtonConstraints.setX(Spring.sum(
                Spring.constant(5),
                jTextFieldConstraints.getConstraint(SpringLayout.EAST)));
        jButtonConstraints.setY(Spring.sum(
                Spring.constant(5),
                jScrollPaneConstrains.getConstraint(SpringLayout.SOUTH)));

        //Adjust constraints for the content pane.
        Component[] components = contentPane.getComponents();
        Spring maxHeightSpring = Spring.constant(0);
        Spring maxWidthSpring = Spring.constant(0);
        SpringLayout.Constraints jFrameConstraints = layout.getConstraints(contentPane);

        //Set the container's right edge to the right edge of its rightmost component + padding.
        Component rightmost = components[components.length - 1];
        SpringLayout.Constraints rightmostConstraints = layout.getConstraints(rightmost);
        jFrameConstraints.setConstraint(
                SpringLayout.EAST,
                Spring.sum(
                        Spring.constant(5),
                        rightmostConstraints.getConstraint(SpringLayout.EAST)));

        //Set the container's bottom edge to the bottom edge of its lowermost component + padding.
        for (Component component : components) {
            SpringLayout.Constraints constraints = layout.getConstraints(component);
            maxHeightSpring = Spring.max(
                    maxHeightSpring,
                    constraints.getConstraint(SpringLayout.SOUTH));
            maxWidthSpring = Spring.max(
                    maxWidthSpring,
                    constraints.getConstraint(SpringLayout.EAST));
        }
        jScrollPaneConstrains.setConstraint(
                SpringLayout.WEST,
                Spring.constant(5));
        jScrollPaneConstrains.setConstraint(
                SpringLayout.EAST,
                Spring.sum(
                        Spring.constant(0),
                        maxWidthSpring));
        jButtonConstraints.setConstraint(
                SpringLayout.NORTH,
                Spring.sum(Spring.constant(-30), maxHeightSpring));
        jButtonConstraints.setConstraint(
                SpringLayout.SOUTH,
                maxHeightSpring);
        jTextFieldConstraints.setConstraint(
                SpringLayout.NORTH,
                Spring.sum(
                        Spring.constant(-30),
                        maxHeightSpring));
        jTextFieldConstraints.setConstraint(
                SpringLayout.SOUTH,
                maxHeightSpring);
        jFrameConstraints.setConstraint(
                SpringLayout.SOUTH,
                Spring.sum(
                        Spring.constant(5),
                        maxHeightSpring));
        jFrameConstraints.setConstraint(
                SpringLayout.EAST,
                Spring.sum(
                        Spring.constant(5),
                        maxWidthSpring));

        //Display the window.
        setVisible(true);
        getContentPane().setPreferredSize(new Dimension(400, 500));
        pack();
        getContentPane().setMinimumSize(new Dimension(100, 120));
        setMinimumSize(new Dimension(100, 120));
    }

    private void actionAddingElement(JTextField jTextField, ChildWindowController childWindowController) {
        String element = jTextField.getText();
        childWindowController.addElement(element);
        jTextField.setText("");
    }

    public void refreshData() {
        revalidate();
        jScrollPaneVertical.setValue(jScrollPaneVertical.getMaximum());
        jPanel.repaint();
    }

    public void showAllData(List<String> data) {
        for (String element : data) {
            jPanel.add(new JLabel(element));
        }
    }

    public void showNewElement(String element) {
        jPanel.add(new JLabel(element));
    }

    public void showWarningMessage() {
        JOptionPane.showMessageDialog(
                null, "Cannot add empty element",
                "Warning", JOptionPane.WARNING_MESSAGE);
    }
}
